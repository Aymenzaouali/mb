import React, { useContext, useState, useEffect } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core/styles';
import { DarkTheme } from './theme/dark';
import { LightTheme } from './theme/light';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './components/header';

import Training from './pages/training';
import Modules from './pages/modules';
import Home from './pages/home';
import Academie from './pages/academie';
import GetStarted from './pages/get-started';

function App() {
  const [theme, setTheme] = useState(DarkTheme);

  // we change the palette type of the theme in state
  const toggleDarkTheme = (status) => {
    setTheme(status ? DarkTheme : LightTheme);
  };

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />

      <Router>
        <>
          <Header toggleDarkTheme={toggleDarkTheme} />
          <Switch>
            <Route path="/training/:trainingId/modules/:moduleId">
              <Modules />
            </Route>

            <Route path="/training/:trainingId">
              <Training />
            </Route>

            <Route path="/get-started">
              <GetStarted />
            </Route>

            <Route path="/academie">
              <Academie />
            </Route>

            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </>
      </Router>
    </ThemeProvider>
  );
}

export default App;
