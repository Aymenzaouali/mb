import React, { memo } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import { makeStyles } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import LazyLoad from 'react-lazyload';
import CircularProgress from '@material-ui/core/CircularProgress';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  item: {
    flexBasis: 310,
    margin: 15,
    [theme.breakpoints.down('md')]: {
      flexBasis: '45%',
    },
    [theme.breakpoints.down('sm')]: {
      flexBasis: '100%',
    },
  },
  overlay: {
    position: 'absolute',
    height: 415,
    width: 310,
    borderRadius: 9,
    overflow: 'hidden',
    [theme.breakpoints.down('sm')]: {
      width: '93vw',
    },
  },
  itemContent: {
    height: 415,
    backgroundSize: 'cover',
    textAlign: 'center',
    boxShadow: '0px 3px 8px rgba(0,0,0, 0.06)',
    borderRadius: 10,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
}));

function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="static" color="secondary" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          variant="caption"
          component="div"
          color="textSecondary"
        >{`${Math.round(props.value)}`}</Typography>
      </Box>
    </Box>
  );
}

function TrainingCard({ index, image }) {
  const classes = useStyles();

  return (
    <div className={classes.item}>
      <Link to="/training/identify-the-7-toxic-personality-profiles">
        <div className={classes.itemContent}>
          <img src={image} width="100%" alt="" />
          <div className={classes.overlay}>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'space-between',
                height: '100%',
              }}
            >
              <h4
                style={{
                  color: '#fff',
                  margin: 0,
                  fontSize: 15,
                  background:
                    'linear-gradient(to top,  rgba(0, 0, 0, 0) 0%,  rgba(0, 0, 0, 1) 115%)',
                  height: 100,
                  paddingTop: 10,
                }}
              >
                Marco Tamburini
              </h4>
              <h5
                style={{
                  color: '#fff',
                  margin: 0,
                  fontSize: 18,
                  background:
                    'linear-gradient(to bottom,  rgba(0, 0, 0, 0) 0%,  rgba(0, 0, 0, 1) 115%)',
                  paddingBottom: 10,
                  padding: 15,
                }}
              >
                Les 6 étapes clés du professionnel de la vente
              </h5>
            </div>
          </div>
        </div>

        <div
          style={{
            background: 'black',
            borderTop: '5px solid #dd9933',
            padding: 15,
            marginTop: -5,
            position: 'relative',
            borderBottomLeftRadius: 10,
            borderBottomRightRadius: 10,
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <div>
            <p style={{ color: '#fff', margin: 0, fontWeight: 700 }}>Vente</p>
            <span style={{ color: '#fff', margin: 0 }}>75 mn</span>
          </div>
          <div>
            <CircularProgressWithLabel value={60} />
          </div>
        </div>
      </Link>
    </div>
  );
}

export default memo(TrainingCard);
