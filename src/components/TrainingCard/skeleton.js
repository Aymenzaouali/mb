import React, { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Skeleton from '@material-ui/lab/Skeleton';

const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
  item: {
    flexBasis: '22%',
    margin: 15,
    borderRadius: 5,
    border: '1px solid rgba(0,0,0,0.08)',
    boxShadow: '0px 3px 8px rgba(0,0,0, 0.06)',
  },
});

function TrainingCardSkeleton() {
  const classes = useStyles();

  return (
    <div className={classes.item}>
      <Skeleton variant="rect" width={'100%'} height={430} />
    </div>
  );
}

export default memo(TrainingCardSkeleton);
