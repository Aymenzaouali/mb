import React, { useState, useEffect, useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Icon from '@material-ui/core/Icon';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Avatar from '@material-ui/core/Avatar';
import Switch from '@material-ui/core/Switch';
import MasterBusinessDark from '../../assets/images/master-business-dark.png';
import MasterBusinessLight from '../../assets/images/master-business-light.png';
import { useTheme } from '@material-ui/core/styles';

import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.light,
    height: 90,
    justifyContent: 'space-between',
    position: 'fixed',
    zIndex: 10,
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    borderBottom: '1px solid rgba(0,0,0,0.08)',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
  },
  logo: {
    margin: 25,
  },
  search: {
    width: 300,
    [theme.breakpoints.down('md')]: {
      display: 'none',
      width: 0,
    },
  },
  icn: {
    color: '#848484',
    position: 'relative',
    top: 5,
    [theme.breakpoints.down('md')]: {
      display: 'none'
    },
  },
}));

export default function Header({ toggleDarkTheme }) {
  const classes = useStyles();
  const theme = useTheme();
  const [anchorEl, setAnchorEl] = useState(null);
  const [state, setState] = useState({
    checkedA: true,
    checkedB: true,
  });

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
    toggleDarkTheme(event.target.checked);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
 
    <div className={classes.root}>
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <Link to="/">
         {theme.palette.type ==="dark"?(
          <img
            src={MasterBusinessDark}
            className={classes.logo}
            width="95"
            alt=""
          />
         ):(
          <img
          src={MasterBusinessLight}
          className={classes.logo}
          width="95"
          alt=""
        />
         ) }
        </Link>
        <div style={{ marginLeft: 20 }}>
          <Icon className={classes.icn}>search</Icon>
          <TextField
            className={classes.search}
            placeholder="Que voulez-vous apprendre aujourd'hui ?"
          />
        </div>
      </div>

      <div style={{ display: 'flex' }}>
        <Switch
          checked={state.checkedA}
          onChange={handleChange}
          name="checkedA"
          inputProps={{ 'aria-label': 'secondary checkbox' }}
        />
        
        <Avatar
          alt="Remy Sharp"
          src="https://material-ui.com/static/images/avatar/1.jpg"
          aria-haspopup="true"
          onClick={handleClick}
          style={{ marginRight: 20 }}
        />

        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>Profile</MenuItem>
          <MenuItem onClick={handleClose}>My account</MenuItem>
          <MenuItem onClick={handleClose}>Logout</MenuItem>
        </Menu>
      </div>
    </div>
  );
}
