import React, { useEffect } from 'react';

import Menu from '../menu';
import { makeStyles } from '@material-ui/core/styles';
import Hidden from '@material-ui/core/Hidden';

const useStyles = makeStyles((theme) => ({
  root: {
    background: theme.palette.primary.light,
  },
  content: {
    marginLeft: 150,
    paddingTop: 85,
    [theme.breakpoints.down('md')]: {
      marginLeft: 0,
    },
  },
}));

export default function WithMenu(props) {
  const classes = useStyles();

  useEffect(() => {
    window.scrollTo(0, 0);
  }, []);
  return (
    <div className={classes.root}>
      <Hidden mdDown>
        <Menu />
      </Hidden>
      <div className={classes.content}>{props.children}</div>
    </div>
  );
}
