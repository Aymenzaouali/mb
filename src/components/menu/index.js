import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MasterBusinessDark from '../../assets/images/master-business-dark.png';
import Icon from '@material-ui/core/Icon';
import { Link } from 'react-router-dom';

const useStyles = makeStyles((theme) => ({
  root: {
    width: 150,
    background: theme.palette.primary.light,
    height: 345,
    position: 'fixed',
    top: 0,
    borderRight: '1px solid rgba(0, 0, 0, 5)',
    boxShadow: '0 0 15px rgba(0, 0, 0, .9)',
    zIndex: 9
  },
  logo: {
    margin: 25,
  },
  item: {
    textAlign: 'center',
    borderBottom: '1px dashed #848484',
    margin: 10,
    color: '#848484',
  },
  title: {
    marginBottom: 10,
    marginTop: 0,
    fontSize: 12,
  },
  icn: {
    fontSize: 40,
  },
}));

export default function Menu() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <div style={{height:80}}>
      {/*   <img
          src={MasterBusinessDark}
          className={classes.logo}
          width="95"
          alt=""
        /> */}
      </div>
      <div className={classes.item}>
        <Link to="/" style={{ color: '#848484' }}>
          <>
            <Icon className={classes.icn}>ondemand_video</Icon>
            <h3 className={classes.title}>Formations</h3>
          </>
        </Link>
      </div>
      <div className={classes.item}>
        <Link to="/get-started" style={{ color: '#848484' }}>
          <>
            <Icon className={classes.icn}>movie_creation</Icon>
            <h3 className={classes.title}>Bien démarrer</h3>
          </>
        </Link>
      </div>
      <div className={classes.item} style={{ border: 'none' }}>
        <Link to="" style={{ color: '#848484' }}>
          <>
            <Icon className={classes.icn}>apartment</Icon>
            <h3 className={classes.title}>Qui sommes-nous ?</h3>
          </>
        </Link>
      </div>
    </div>
  );
}
