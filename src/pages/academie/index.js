import React, { memo, lazy, Suspense } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
// import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import TrainingCardSkeleton from '../../components/TrainingCard/skeleton';
import LayoutWithMenu from '../../components/layout/withMenu';

const TrainingCard = lazy(() => import('../../components/TrainingCard'));

/* const useStyles = makeStyles({
  root: {
    flexGrow: 1,
  },
}); */

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <h2>{children}</h2>
        </Box>
      )}
    </div>
  );
}

const renderLoader = () => <TrainingCardSkeleton />;
const data = [
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/06/SCREENSHOT_CUAU_ARAU_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/nancy-marcoux-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/juan-carlos-gonzalez-season-01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/06/alex-dey-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/erick-gamio-screen-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/03/EV_Quintero_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/olivier-madelrieux-course-cover-02.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/muriel-duhem-seaoson-01-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/omar-rueda-season-01-500x705.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/02/Olga-Ciesco-Master-Business-500x705-1-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/05/PONCHO_ROBLES_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/04/SCREEN-500x705_LOUIS_LAURENT_C20-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
];

const otherData = [
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/02/Margarita_Arias_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/kim-bennour-background.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/juan-carlos-gonzalez-season-01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/06/alex-dey-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/erick-gamio-screen-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/03/EV_Quintero_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/olivier-madelrieux-course-cover-02.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/muriel-duhem-seaoson-01-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/omar-rueda-season-01-500x705.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/02/Olga-Ciesco-Master-Business-500x705-1-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/05/PONCHO_ROBLES_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/04/SCREEN-500x705_LOUIS_LAURENT_C20-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
];

function Academie() {
  // const classes = useStyles();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <LayoutWithMenu>
      <Tabs
        value={value}
        onChange={handleChange}
        style={{ marginLeft: 50 }}
        indicatorColor="primary"
        variant="scrollable"
        scrollButtons="auto"
      >
        <Tab label="All courses" {...a11yProps(0)} />
        <Tab label="OMV" {...a11yProps(1)} />
        <Tab label="Watchlist" {...a11yProps(2)} />
        <Tab label="Favorites" {...a11yProps(3)} />
        <Tab label="Reviews" {...a11yProps(4)} />
      </Tabs>
      <div>
        <TabPanel value={value} index={0}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {data.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </TabPanel>

        <TabPanel value={value} index={1}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {data.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </TabPanel>

        <TabPanel value={value} index={2}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {data.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {data.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </TabPanel>
        <TabPanel value={value} index={4}>
          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {otherData.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </TabPanel>
      </div>
    </LayoutWithMenu>
  );
}
export default memo(Academie);
