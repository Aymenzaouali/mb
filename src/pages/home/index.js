import React, { memo, lazy, Suspense } from 'react';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader
import { Carousel } from 'react-responsive-carousel';
import ReactPlayer from 'react-player';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import LayoutWithMenu from '../../components/layout/withMenu';
import LazyLoad from 'react-lazyload';
import TrainingCardSkeleton from '../../components/TrainingCard/skeleton';

const TrainingCard = lazy(() => import('../../components/TrainingCard'));

const data = [
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/erick-gamio-screen-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/03/EV_Quintero_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/olivier-madelrieux-course-cover-02.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/muriel-duhem-seaoson-01-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
];
const renderLoader = () => <TrainingCardSkeleton />;

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  description: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      flexDirection: 'column',
    },
  },
  sectionTitle: {
    color: theme.palette.text.primary,
    textAlign: 'center',
    margin: 35,
    '&::before': {
      content: '&nbsp;',
      width: '20%',
      height: 3,
      backgroundColor: '#dd9933',
      position: 'absolute',
      top: 50,
      textAlign: 'center',
      left: '40%',
      fontSize: 0,
    },
  },
  legend: {
    position: 'absolute',
    bottom: '35%',
    left: '50%',
    marginLeft: '-45%',
    textAlign: 'left',
    width: '60%',
    background: 'none',
    [theme.breakpoints.down('md')]: {
      bottom: '20%',
    },
  },
  titleLegend: {
    color: '#fff',
    fontWeight: 500,
    fontSize: 45,
    margin: 0,
    [theme.breakpoints.down('md')]: {
      fontSize: 30,
    },
  },
  detailLegent: {
    fontSize: 13,
    color: 'rgba(255,255,255, 0.65)',
  },
  formatLegend: {
    color: 'rgba(255,255,255, 0.8)',
    borderColor: 'rgba(255,255,255, 0.8)',
    display: 'inline-block',
    lineHeight: '1',
    margin: '0px 7px 5px 0px',
    padding: 7,
    borderRadius: 5,
    fontSize: 13,
    fontWeight: '700',
    border: '2px solid #9d9d9d',
  },
  overlay: {
    backgroundImage:
      'linear-gradient( to right, rgba(0, 0, 0, 0.8), rgba(0, 0, 0, 0.6), rgba(0, 0, 0, 0), rgba(0, 0, 0, 0) )',
    width: '100%',
    height: 385,
    position: 'absolute',
  },
}));

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <h2>{children}</h2>
        </Box>
      )}
    </div>
  );
}

const firstCoach = [
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/06/SCREENSHOT_CUAU_ARAU_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/nancy-marcoux-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/08/juan-carlos-gonzalez-season-01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/06/alex-dey-season01.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
];

const secondCoach = [
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/11/omar-rueda-season-01-500x705.png',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/02/Olga-Ciesco-Master-Business-500x705-1-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/05/PONCHO_ROBLES_500x705-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
  {
    coachName: 'Marco Tamburini ',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/04/SCREEN-500x705_LOUIS_LAURENT_C20-500x705.jpg',
    title: 'Les 6 étapes clés du professionnel de la vente',
  },
];

const slider = [
  {
    title: 'Décodez le langage corporel de votre interlocuteur',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/05/olga-ciesco-1920x698-1.png',
  },
  {
    title: 'Perdez du poids grâce à la nutrition fonctionnelle',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2020/05/anne-josie-roy-slider-01.jpg',
  },
  {
    title: 'Augmentez votre thermostat financier',
    image:
      'https://static.masterbusiness.com/wp-content/uploads/2019/09/olivier-madelrieux-slider.jpeg',
  },
];

function Home() {
  const classes = useStyles();

  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <LayoutWithMenu>
      <>
        <div>
          <Carousel
            infiniteLoop
            showArrows={false}
            showStatus={false}
            showThumbs={false}
          >
            {slider.map((item, index) => (
             
                <div key={index}>
                  <div className={classes.overlay} />

                  <img
                    src={item.image}
                    style={{ objectFit: 'cover', height: 385 }}
                    alt=""
                  />

                  <div className={classes.legend}>
                    <span className={classes.formatLegend}>Full HD</span>
                    <p className={classes.titleLegend}>{item.title}</p>
                    <span className={classes.detailLegent}>
                      27 septembre, 2019 | 57 mn | Développement personnel |
                      Procrastination
                    </span>
                  </div>
                </div>
             
            ))}
          </Carousel>
        </div>

        <div>
          <h2 className={classes.sectionTitle}>Top Nouveautés</h2>

          <div style={{ display: 'flex', flexWrap: 'wrap' }}>
            {firstCoach.map((item, index) => (
              <Suspense key={index} fallback={renderLoader()}>
                <TrainingCard index={index} image={item.image} />
              </Suspense>
            ))}
          </div>
        </div>

        <div>
          <h2 className={classes.sectionTitle}>
            Découvrez le "Netflix" de la formation
          </h2>

          <div className={classes.description}>
            <div style={{ flex: 1 }}>
              <ReactPlayer
                className="react-player"
                url="https://vimeo.com/396293836"
                width="90%"
                height="350px"
                style={{ margin: 20 }}
                controls
              />
            </div>

            <div style={{ flex: 1 }}>
              <p
                style={{
                  color: 'lightgrey',
                  fontSize: 19,
                  lineHeight: '28px',
                  marginTop: 20,
                }}
              >
                DLM News, partenaire de BFM Business et Challenges, nous
                interviewe lors du Salon Learning Technologies de Paris 2020.
                Mehdi Moustaoui, cofondateur de Master Business répond aux
                questions d’Alexia Borg. Découvrez-y notre mission, notre vision
                et nos objectifs.
              </p>
            </div>
          </div>
        </div>

        <div>
          <h2 className={classes.sectionTitle}>Toutes les formations</h2>
          <Tabs
            value={value}
            onChange={handleChange}
            indicatorColor="primary"
            centered
            variant="scrollable"
            scrollButtons="auto"
          >
            <Tab label="Item One" {...a11yProps(0)} />
            <Tab label="Item Two" {...a11yProps(1)} />
          </Tabs>
          <div>
            <TabPanel value={value} index={0}>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {data.map((item, index) => (
                  <Suspense key={index} fallback={renderLoader()}>
                    <TrainingCard index={index} image={item.image} />
                  </Suspense>
                ))}
              </div>
            </TabPanel>

            <TabPanel value={value} index={1}>
              <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                {secondCoach.map((item, index) => (
                  <Suspense key={index} fallback={renderLoader()}>
                    <TrainingCard index={index} image={item.image} />
                  </Suspense>
                ))}
              </div>
            </TabPanel>
          </div>
        </div>
      </>
    </LayoutWithMenu>
  );
}

export default memo(Home);
