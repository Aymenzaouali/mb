import React, { useEffect } from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import CircularProgress from '@material-ui/core/CircularProgress';
import Player from './player';
import { Route, NavLink, useLocation, Link } from 'react-router-dom';
import 'react-perfect-scrollbar/dist/css/styles.css';
import ScrollBar from 'react-perfect-scrollbar';
import Hidden from '@material-ui/core/Hidden';

import Coach from '../../assets/images/coach.jpg';

const useStyles = makeStyles(theme=>({
  root: {
    flexGrow: 1,
    background: theme.palette.primary.light
  },
  coach: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    padding: 20,
  },
  coachAvatar: {
    width: 150,
    border: '2px solid #9a6b23',
    borderRadius: '50%',
  },
  coachName: {
    background: '#091219',
    position: 'relative',
    marginTop: -40,
    padding: '0 35px',
    textAlign: 'center',
    border: '2px solid #9a6b23',
    borderRadius: 5,
    fontSize: 18,
    color: '#fff',
    margin: 20,
    marginBottom: 35,
  },
  trainingTitle: {
    fontSize: 20,
  
    textAlign: 'center',
    margin: 0,
  },
  moduleTitle: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '11px 0',
    borderBottom: '1px dashed #ffff',
    width: '100%',
  },
  backToAcadmy: {
    position: 'fixed',
    cursor: 'pointer',
    whiteSpace: 'nowrap',
    borderRadius: 10,
    border: '2px solid #9a6b23',
    borderWidth: '3px 3px 3px 3px',
    padding: '5px 20px 5px 20px',
    color: '#9a6b23',
    fontSize: 15,
    fontWeight: '600',
    fontStyle: 'normal',
    background: '#091219',
    bottom: 20,
    left: 20,
    [theme.breakpoints.down('md')]: {
      position: 'relative',
      bottom: -20,
    },
  },
}));

function RouteWithSubRoutes(route) {
  return (
    <Route
      path={route.path}
      render={(props) => (
        // pass the sub-routes down to keep nesting
        <route.component {...props} routes={route.routes} title={route.title} />
      )}
    />
  );
}

function a11yProps(index) {
  return {
    id: `nav-tab-${index}`,
    'aria-controls': `nav-tabpanel-${index}`,
  };
}

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`nav-tabpanel-${index}`}
      aria-labelledby={`nav-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <h2>{children}</h2>
        </Box>
      )}
    </div>
  );
}

const modulesArr = [
  {
    path: '/training/identify-the-7-toxic-personality-profiles/modules/1',
    title: 'Intro: Bienvenue dans cette classe',
    component: Player,
  },
  {
    path: '/training/identify-the-7-toxic-personality-profiles/modules/2',
    title: 'Intro: Bienvenue dans cette classe',
    component: Player,
  },
  {
    path: '/training/identify-the-7-toxic-personality-profiles/modules/3',
    title: 'Intro: Bienvenue dans cette classe',
    component: Player,
  },
  {
    path: '/training/identify-the-7-toxic-personality-profiles/modules/4',
    title: 'Intro: Bienvenue dans cette classe',
    component: Player,
  },
  {
    path: '/training/identify-the-7-toxic-personality-profiles/modules/5',
    title: 'Intro: Bienvenue dans cette classe',
    component: Player,
  },
];

function CircularProgressWithLabel(props) {
  return (
    <Box position="relative" display="inline-flex">
      <CircularProgress variant="static" {...props} />
      <Box
        top={0}
        left={0}
        bottom={0}
        right={0}
        position="absolute"
        display="flex"
        alignItems="center"
        justifyContent="center"
      >
        <Typography
          variant="caption"
          component="div"
          color="textSecondary"
        >{`${Math.round(props.value)}%`}</Typography>
      </Box>
    </Box>
  );
}

export default function Modules() {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [progress, setProgress] = React.useState(10);
  const location = useLocation();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  useEffect(() => {
    window.scrollTo(0, 0);
   // props.enabledView(false);
  }, []);

  useEffect(() => {
   /*  const timer = setInterval(() => {
      setProgress((prevProgress) =>
        prevProgress >= 100 ? 10 : prevProgress + 10
      );
    }, 800);
    return () => {
      clearInterval(timer);
    }; */
  }, []);

  return (
    <div className={classes.root}>
      <Grid container>
        <Grid item lg={3} md={3} sm={12} xs={12}>
          <div
            style={{
              boxShadow: '0 0 15px rgba(0, 0, 0, .9)',
              height: '100vh',
              marginTop: 90
            }}
          >
            <div className={classes.coach}>
              <img src={Coach} className={classes.coachAvatar} alt="" />
              <span className={classes.coachName}>Anne-Josie Roy</span>
            </div>
            <h2 className={classes.trainingTitle}>
              Perdez du poids grâce à la nutrition fonctionnelle
            </h2>
            <Tabs
              value={value}
              onChange={handleChange}
              indicatorColor="secondry"
              textColor="secondry"
              centered
            >
              <Tab label="MODULES" {...a11yProps(0)} />
              <Tab label="DETAILS" {...a11yProps(1)} />
            </Tabs>
            <div>
              <TabPanel
                value={value}
                index={0}
                style={{ overflowY: 'scroll', height: '35vh' }}
              >
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                  <ScrollBar style={{ width: '100%' }}>
                    {modulesArr.map((item) => (
                      <div className={classes.moduleTitle}>
                        <NavLink
                          to={item.path}
                          activeStyle={{
                            fontWeight: 'bold',
                            color: 'red',
                            flex: 3.5,
                          }}
                        >
                          <span style={{ fontSize: 15 }}>
                            {item.title}
                          </span>
                        </NavLink>

                        {location.pathname === item.path && (
                          <CircularProgressWithLabel value={progress} />
                        )}
                      </div>
                    ))}
                  </ScrollBar>
                </div>
              </TabPanel>

              <TabPanel value={value} index={1}>
                <div style={{ display: 'flex', flexWrap: 'wrap' }}>
                  <p>Two</p>
                </div>
              </TabPanel>
              <Link to="/academie" className={classes.backToAcadmy}>
                Back to the academy
              </Link>
            </div>
          </div>
        </Grid>
       
        <Grid item lg={9} md={9} sm={12} xs={12}>
          <div style={{ marginTop: 120 }}>
            {modulesArr.map((route, i) => (
              <RouteWithSubRoutes key={i} {...route} />
            ))}
          </div>
        </Grid>
       
      </Grid>
    </div>
  );
}
