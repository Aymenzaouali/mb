import React, { useState } from 'react';
import ReactPlayer from 'react-player';
import CircularProgress from '@material-ui/core/CircularProgress';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    background: theme.palette.primary.main
  },
  loadingPlayer: {
    position: 'absolute',
    background: theme.palette.primary.light,
    width: '72vw',
    height: '100vh',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));

export default function Player() {
  const [ready, setReady] = useState(true);
  const classes = useStyles();

  return (
    <div>
      {ready && (
        <div className={classes.loadingPlayer}>
          <CircularProgress />
        </div>
      )}
      <ReactPlayer
        url="https://vimeo.com/396293836"
        width="100%"
        height="600px"
        onProgress={(data) => console.log(data)}
        onReady={() => setReady(false)}
      />

      <p style={{ color: '#fff',margin: 40 }}>
        Pendant ces 13 années d’expérience, j’ai bien compris les difficultés
        qu’ont les personnes à augmenter leurs revenus dans le monde du MLM. Ils
        ont du mal à trouver de nouveaux prospects, à faire des appels, ils ne
        font pas de suivi professionnel et si par miracle ils signent un nouvel
        associé, ils ne savent pas comment en faire un leader potentiel grâce à
        un démarrage réellement explosif. Dans cette formation, je vous
        enseignerai à travers ces 4 modules à vous convertir en networker
        professionnel. Vous apprendrez à avoir l’attitude d’un entrepreneur, à
        prospecter sans ressembler à un marchand de tapis, vous saurez comment
        former des leaders de haut niveau, pour enfin atteindre la liberté
        financière tant espérée.
      </p>
    </div>
  );
}
