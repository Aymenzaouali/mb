import React, { memo } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import ReactPlayer from 'react-player';
import { Link } from 'react-router-dom';
import Rating from '@material-ui/lab/Rating';
import Avatar from '@material-ui/core/Avatar';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import Icon from '@material-ui/core/Icon';
import Dialog from '@material-ui/core/Dialog';
import Slide from '@material-ui/core/Slide';
import LayoutWithMenu from '../../components/layout/withMenu';

import erickGamio from '../../assets/images/erick-gamio-screen.png';

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  sideBar: {
    padding: theme.spacing(2),
    textAlign: 'center',
    background: theme.palette.primary.light,
    top: 90,
    height: '100%',
    zIndex: -6,
    borderRight: '1px solid rgba(0, 0, 0, 5)',
    boxShadow: '0 0 15px rgba(0, 0, 0, .9)',
  },
  paper: {
    padding: theme.spacing(2),
    color: theme.palette.text.secondary,
  },
  startButton: {
    cursor: 'pointer',
    display: 'inline-block',
    whiteSpace: 'nowrap',
    background: '#9a6b23',
    borderRadius: 10,
    border: '3px solid #9a6b23',
    borderWidth: 3,
    padding: '5px 20px',
    color: '#ffffff',
    fontSize: 16,
    fontWeight: '900',
    fontStyle: 'normal',
  },
}));

function Training(props) {
  const classes = useStyles();
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  return (
    <LayoutWithMenu>
      <Grid container>
        <Grid item lg={3} md={3}>
          <div className={classes.sideBar}>
            <div
              style={{
                justifyContent: 'center',
                display: 'flex',
                alignItems: 'flex-end',
              }}
            >
              <div style={{ position: 'absolute' }}>
                <Icon
                  style={{ fontSize: 45, marginBottom: -25 }}
                  color="primary"
                  onClick={handleClickOpen}
                >
                  play_circle_outline
                </Icon>
              </div>
              <img src={erickGamio} width="100%" alt="" />
            </div>

            <h1 style={{ fontSize: 25, marginTop: 25 }}>
              Perdez du poids grâce à la nutrition fonctionnelle
            </h1>
            <Link
              to="/training/identify-the-7-toxic-personality-profiles/modules/1"
              className={classes.startButton}
            >
              SUIVRE LA FORMATION
            </Link>

            <Rating
              name="read-only"
              value={9}
              max={10}
              style={{ margin: '20px 0' }}
              readOnly
            />

            <div style={{ textAlign: 'left' }}>
              <p>
                <b>DATE DE SORTIE:</b> <span>30 mai, 2020</span>
              </p>
              <p>
                <b>DURÉE:</b> <span>92 min</span>
              </p>
              <p>
                <b>LIVEL:</b> <span>Intermediate</span>
              </p>
              <p>
                <b>STATE:</b> <span>PENDING</span>
              </p>
            </div>
          </div>
        </Grid>
        <Grid item lg={9} md={9}>
          <div className={classes.paper}>
            <h1>Bienvenue dans cette formation de Master Business</h1>
            <p>
              Comment vous y retrouver avec toutes les informations qui
              circulent dans le domaine de l’alimentation ? Faites le point sur
              votre alimentation et perdez du poids grâce à la nutrition
              fonctionnelle !
            </p>
            <h2>Ils ont adoré cette formation !</h2>
            <AvatarGroup max={4}>
              <Avatar
                alt="Remy Sharp"
                src="https://material-ui.com/static/images/avatar/1.jpg"
              />
              <Avatar
                alt="Travis Howard"
                src="https://material-ui.com/static/images/avatar/2.jpg"
              />
              <Avatar
                alt="Cindy Baker"
                src="https://material-ui.com/static/images/avatar/3.jpg"
              />
              <Avatar
                alt="Agnes Walker"
                src="https://material-ui.com/static/images/avatar/1.jpg"
              />
              <Avatar
                alt="Trevor Henderson"
                src="https://material-ui.com/static/images/avatar/2.jpg"
              />
            </AvatarGroup>

            <h2>Ce que pensent nos membres de cette formation</h2>

            {[1, 2, 3, 4].map((item, index) => (
              <div
                key={index}
                style={{
                  display: 'flex',
                  borderBottom: '1px dashed #eee',
                  padding: '15px 0',
                }}
              >
                <div>
                  <Avatar
                    alt="Remy Sharp"
                    src="/static/images/avatar/1.jpg"
                    style={{ width: 65, height: 65 }}
                  />
                </div>
                <div style={{ marginLeft: 10 }}>
                  <p style={{ margin: 0 }}>
                    Gregory{' '}
                    <Rating
                      name="read-only"
                      value={9}
                      max={10}
                      style={{ fontSize: 12 }}
                      readOnly
                    />
                  </p>
                  <p style={{ margin: 0 }}>28/05/2020</p>
                  <p style={{ margin: 0 }}>Merci pour cette vue sur le MLM</p>
                </div>
              </div>
            ))}

            <div>
              <div>
                <Avatar alt="Remy Sharp" src="/static/images/avatar/1.jpg" />
              </div>
              <div>
                <h2>user name</h2>
                <h2>Laisser un commentaire sur cette formation</h2>
                <p>
                  Avez-vous suivi cette formation ? Vous l'avez aimé ?
                  Commentez-là !
                </p>
              </div>
            </div>

            <div>
              <h2>En savoir plus sur Erick Gamio</h2>
              <div>
                <div></div>

                <div>
                  <h3>
                    Expert et consultant spécialisé en marketing de réseau
                  </h3>
                  <p>
                    Erick Gamio est un des leaders et consultants les plus
                    influents de l’industrie du marketing de réseau en Amérique
                    latine. Avec plus de 10 ans d’expérience, Erick affirme sur
                    son blog, son canal Youtube et ses réseaux sociaux où plus
                    de 100,000 followers le suivent au quotidien, que sa mission
                    professionnelle est la suivante : […]
                  </p>
                </div>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
      <Dialog
        open={open}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <ReactPlayer
          url="https://vimeo.com/396293836"
          width="600px"
          height="335px"
          controls
        />
      </Dialog>
    </LayoutWithMenu>
  );
}

export default memo(Training);
