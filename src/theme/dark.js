import { createMuiTheme } from '@material-ui/core/styles';

export const DarkTheme = createMuiTheme({
  palette: {
    type: 'dark',
    primary: { main: '#091219', light: ' #091219' },
    secondary: { main: '#dd9933' },
    background: {
      default: '#091219',
    },
    text: {
      primary: '#ffffff',
      secondary: '#ffffff',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)}',
    },
  },
});
