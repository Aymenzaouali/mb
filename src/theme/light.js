import { createMuiTheme } from '@material-ui/core/styles';

export const LightTheme = createMuiTheme({
  palette: {
    type: 'light',
    primary: { main: '#dd9933', light: '#fff' },
    secondary: { main: '#dd9933' },
    background: {
      paper: '#fff',
    },
    text: {
      primary: '#091219',
      secondary: '#091219',
      disabled: 'rgba(0, 0, 0, 0.38)',
      hint: 'rgba(0, 0, 0, 0.38)}',
    },
  },
});
